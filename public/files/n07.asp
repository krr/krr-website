<pre>
<span class="comment">% Derive "types" from the input predicates.</span>
truck(T) :- fuel(T,_).
package(P) :- at(P,L), not truck(P).
location(L) :- fuelcost(_,L,_).
location(L) :- fuelcost(_,_,L).
locatable(O) :- at(O,L).

<span class="comment">% Derive the state at time 0 from the input predicates.</span>
at(O,L,0) :- at(O,L).
fuel(T,F,0) :- fuel(T,F).
 
<span class="comment">% GENERATE: at every timepoint, exaclty one of unload, load, drive and noop with correct types should be chosen. Unless other rules also define one of these predicates, this means that at every time (at most) one action occurs.</span>
1 &le; { unload( P,T,L,S ) : 
	package( P ) , 
 	truck( T ) , 
	location( L ); 
    load( P,T,L,S ) : 
	package( P ) , 
	truck( T ) , 
	location( L ); 
    drive( T,L1,L2,S ) : 
	fuelcost( Fueldelta,L1,L2 ) , 
	truck( T );
    noop(S)
  } &le; 1 :- step(S), S > 0.

<span class="comment">% When an unload action occurs, the package arives at the location specified in the unload action.</span>
at( P,L,S ) :- unload( P,T,L,S ).
<span class="comment">% In this case, we remember that inertia no longer holds for this atom</span>
del( in( P,T ),S ) :- unload( P,T,L,S ).

<span class="comment">% Similar to unload</span>
del( at( P,L ),S ) :- load( P,T,L,S ).
in( P,T,S ) :- load( P,T,L,S ).

<span class="comment">% Similar to unload</span>
del( at( T,L1 ), S ) :- drive( T,L1,L2,S ).
at( T,L2,S ) :- drive( T,L1,L2,S). 

<span class="comment">% The fuel at the next step is calculated in terms of the fuel in the previous steps.</span>
del( fuel( T,Fuelpre ),S ) :- drive( T,L1,L2,S ), fuel(T, Fuelpre,S-1).
fuel( T,Fuelpost,S ) :- drive( T,L1,L2,S ), fuelcost(Fueldelta,L1,L2), fuel(T,Fuelpre,S-1), Fuelpost = Fuelpre - Fueldelta.
 
<span class="comment">% Inertia holds unless something gets "deleted".</span>
at( O,L,S ) :- at( O,L,S-1 ), not del( at( O,L ),S  ), step(S).
in( P,T,S ) :- in( P,T,S-1 ), not del( in( P,T ),S  ), step(S).
fuel( T,Level,S ) :- fuel( T,Level,S-1 ), not del( fuel( T,Level) ,S ), truck( T ), step(S).
 
<span class="comment">% It cannot be the case that an action occurs while not knowing that the preconditions hold.</span>
 :- unload( P,T,L,S ), not preconditions_u( P,T,L,S ).
preconditions_u( P,T,L,S ) :- step(S), at( T,L,S-1 ), in( P,T,S-1 ), package( P ), truck( T ).

 :- load( P,T,L,S ), not preconditions_l( P,T,L,S ).
preconditions_l( P,T,L,S ) :- step(S), at( T,L,S-1 ), at( P,L,S-1 ).

 :- drive( T,L1,L2,S ), not preconditions_d( T,L1,L2,S ).
preconditions_d( T,L1,L2,S ) :- step(S), at( T,L1,S-1 ), fuel( T, Fuelpre, S-1), fuelcost(Fueldelta,L1,L2), Fuelpre - Fueldelta >= 0.

<span class="comment">% The goal is reached if some number both equals the number of packages that are at their location and the number of packages for which a goal location is specified.</span>
goalreached :- step(S),  N = #count{ P,L,S : at(P,L,S) , goal(P,L) }, N = #count{ P,L : goal(P,L) }.
<span class="comment">% It cannot be the case that we don't know that the goal is reached.</span>
:- not goalreached.
</pre>