<pre>
<span class="idpheader">vocabulary V{</span>
<span class="comment">//The input types and predicates</span>
type Xco
type Yco
type Number isa int
type Bottle
xsucc(Xco,Xco)
ysucc(Yco,Yco)
xvaluef(Yco):Number
yvaluef(Xco):Number
bottle(Bottle,Xco,Yco)
<span class="comment">//The output predicate</span>
filled(Xco,Yco)
<span class="idpheader">}</span>
<span class="idpheader">theory T: V{</span>
<span class="comment">//The number of filled cells agrees with the given function.</span>
&forall; x: yvaluef(x) = #{y: filled(x,y)}.
&forall; y: xvaluef(y) = #{x: filled(x,y)}.

<span class="comment">//A cell can only be filled if it is part of a bottle.</span>
&forall; x y: filled(x,y) &rArr; &exist; b: bottle(b,x,y).

<span class="comment">//If a cell is filled, all cells from that bottle with equal height are filled.</span>
&forall; b x y: bottle(b,x,y) &and; filled(x,y) &rArr; &forall; x': bottle(b,x',y) &rArr; filled(x',y).

<span class="comment">//If a cell is filled, all cells from that bottle that are lower, are also filled.</span>
&forall; b x y: bottle(b,x,y) &and; filled(x,y) &rArr; &forall; x' y': ysucc(y,y') &and; bottle(b,x',y') &rArr; filled(x',y').       
<span class="idpheader">}</span>
</pre>
