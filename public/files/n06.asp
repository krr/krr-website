<pre>
<span class="comment">% If there is a bottle on a cell, and I don't know that this cell is unfilled, then it is filled.</span>
filled(X,Y) :- bottle(B,X,Y), not unfilled(X,Y).
<span class="comment">% If there is a bottle on a cell, and I don't know that this cell is filled, then it is unfilled.</span>
unfilled(X,Y) :- bottle(B,X,Y), not filled(X,Y).

<span class="comment">% The number of filled cells agrees with the given predicate.</span>
<span class="comment">% It cannot be the case that <code>xvalue(Y,V)</code>, but I don't know that the number of filled cells on that column is <code>V</code>.</span>
:- xvalue(Y,V), not #count{ X : filled(X,Y) } = V.
<span class="comment">% It cannot be the case that <code>yvalue(X,V)</code>, but I don't know that the number of filled cells on that row is <code>V</code>.</span>
:- yvalue(X,V), not #count{ Y : filled(X,Y) } = V.

<span class="comment">% Two constraints expressing gravity in terms of filled and unfilled.</span>
:- bottle(B,X1,Y1), bottle(B,X2,Y2), ysucc(Y1,Y2), filled(X1,Y1), unfilled(X2,Y2).
:- bottle(B,X1,Y), bottle(B,X2,Y), filled(X1,Y), unfilled(X2,Y), X1 &ne; X2.
</pre>
