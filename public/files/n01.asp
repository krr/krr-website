<pre>
<span class="comment">% Define which are valid indices: 1..patternlength</span>
kval(1).
kval(N+1) :- kval(N), N &lt; L, patternlength(L). 

<span class="comment">% Tell the solver that for every index <code>K</code> one tuple <code>I,E</code> from <code>t</code> should be chosen. For this tuple, <code>subt(K,I,E)</code> is true. </span>
1 &le; { subt(K,I,E) : t(I,E) } &le; 1 :- kval(K), patternlength(L).

<span class="comment">% Constraint expressing that the restriction of subt to the first two arguments respects the order. </span>
:- subt(K1,I1,_), subt(K2,I2,_), K1 &lt; K2, I1 &ge; I2.

<span class="comment">% Solution is obtained from subt by dropping the second argument. </span>
solution(K,E) :- subt(K,_,E).

<span class="comment">% Solution is order-isomorphic to pf.</span>
:- solution(K1,ET1), solution(K2,ET2), p(K1,EP1), p(K2,EP2), ET1 &lt; ET2, EP1 &ge; EP2.
</pre>
