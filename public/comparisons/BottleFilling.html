<h2>Bottle Filling</h2>
<!-- <h3>Problem Description</h3> -->
<hr>
<h4>Description</h4>
<p>
  Given a grid and a number of bottles on the grid, determine how filled each bottle is - each grid element in a bottle may contain liquid or not.
  For each row, respectively column, of the grid the number of filled grid elements is provided.
</p>
<p>
    Gravity acts along the Y coordinate in the direction of the successor relation (i.e. larger Y coordinate is further down), which means that bottles can only either be empty or must have a
    consistent fill level, from the largest Y coordinate to the smallest filled Y coordinate
    in the bottle all grid elements in the bottle with such Y coordinates must be filled (note that this means that
    filled cells are not necessarily linked; requiring this and thereby allowing for different fill levels in different
    areas of the same bottle might be a future elaboration of this problem).
</p>
<p>
    Encodings should not assume bottles to be convex (i.e. they can have "holes"), but bottles can be assumed to be non-overlapping and connected.
</p>
<p>
  The original problem description (from the ASP Competition 2013) can be found <a href="https://www.mat.unical.it/aspcomp2013/BottleFillingProblem">here</a>.
</p>

<h4>Input and output</h4>
Input:
<ul class="bi-ul">
  <li class="in">
    <code>xsucc(x,xs)</code>: specifies the successor relation of the X coordinates; <code>xs</code> is the successor of <code>x</code>.
  </li>
  <li class="in">
    <code>ysucc(y,ys)</code>: specifies the successor relation of the Y coordinates; <code>ys</code> is the successor of <code>y</code>.
  </li>
  <li class="in">
    <code>xvalue(y,v):</code> <code>y</code> is the coordinate of a <span class="keyword" data-tooltip="Note that this is a Y coordinate, as rows are characterized by their Y coordinate">row</span>, <code>v</code> is the number of filled grid elements on that row.
  </li>
  <li class="in">
    <code>yvalue(x,v):</code> <code>x</code> is the coordinate of a <span class="keyword" data-tooltip="Note that this is a X coordinate, as columns are characterized by their X coordinate">column</span>, <code>v</code> is the number of filled grid elements on that column.
  </li>
  <li class="in">
    <code>bottle(b,x,y)</code>: <code>b</code> is a bottle identifier, <code>x</code> and <code>y</code> are coordinates.
</ul>
Output:
<ul class="bi-ul">
  <li class="out">
    The output predicate <code>filled/2</code> should be true for all filled positions in the grid.
  </li>
</ul>

<div class="on-page-nav nav nav-pills d-flex flex-row align-items-baseline justify-content-end" id="bottle-tab" role="tablist" aria-orientation="vertical">
  <h4 style="flex-grow:1;">Models</h4>
  <a class="nav-link active" id="bottle-IDP-tab" data-bs-toggle="pill" href="#bottle-IDP" role="tab" aria-controls="bottle-IDP" aria-selected="true">IDP</a>
  <a class="nav-link" id="bottle-ASP-tab" data-bs-toggle="pill" href="#bottle-ASP" role="tab" aria-controls="bottle-ASP" aria-selected="true">ASP</a>
</div>
<div class="tab-content mt-2">
  <div class="tab-pane fade show active" id="bottle-IDP" role="tabpanel" aria-labelledby="bottle-IDP-tab">
    <!-- <h5>About</h5> -->
    <p>
       In the IDP-model, we chose to replace the predicates <code>xvalue</code> and <code>yvalue</code>
       by functions <code>xvaluef</code> and <code>yvaluef</code> respectively (since in fact, they are functions).
       All constraints are then expressible in a very natural way.
    </p>
    <!-- <h5>Model</h5> -->

<pre><span class="idpheader">vocabulary V{</span>
<span class="comment">//The input types and predicates</span>
type Xco
type Yco
type Number isa int
type Bottle
xsucc(Xco,Xco)
ysucc(Yco,Yco)
xvaluef(Yco):Number
yvaluef(Xco):Number
bottle(Bottle,Xco,Yco)
<span class="comment">//The output predicate</span>
filled(Xco,Yco)
<span class="idpheader">}</span>
<span class="idpheader">theory T: V{</span>
<span class="comment">//The number of filled cells agrees with the given function.</span>
∀ x: yvaluef(x) = #{y: filled(x,y)}.
∀ y: xvaluef(y) = #{x: filled(x,y)}.

<span class="comment">//A cell can only be filled if it is part of a bottle.</span>
∀ x y: filled(x,y) ⇒ ∃ b: bottle(b,x,y).

<span class="comment">//If a cell is filled, all cells from that bottle with equal height are filled.</span>
∀ b x y: bottle(b,x,y) ∧ filled(x,y) ⇒ ∀ x': bottle(b,x',y) ⇒ filled(x',y).

<span class="comment">//If a cell is filled, all cells from that bottle that are lower, are also filled.</span>
∀ b x y: bottle(b,x,y) ∧ filled(x,y) ⇒ ∀ x' y': ysucc(y,y') ∧ bottle(b,x',y') ⇒ filled(x',y').
<span class="idpheader">}</span>
</pre>
    <!-- <h5>Download</h5> -->
    <p>
      <i class="fas fa-file-download"></i> Download the entire IDP-model (including preprocessing of ASP factlists) <a href="files/n06.idp">here</a>.
    </p>
  </div>
  <div class="tab-pane fade" id="bottle-ASP" role="tabpanel" aria-labelledby="bottle-ASP-tab">
    <!-- <h5>About</h5> -->
    <p>
       The ASP-model introduces one extra predicate: <code>unfilled</code>.
       This predicate is the classical negation of <code>filled</code> when restricted to cells that contain bottles.
    </p>
    <!-- <h5>Model</h5> -->

<pre><span class="comment">% If there is a bottle on a cell, and I don't know that this cell is unfilled, then it is filled.</span>
filled(X,Y) :- bottle(B,X,Y), not unfilled(X,Y).
<span class="comment">% If there is a bottle on a cell, and I don't know that this cell is filled, then it is unfilled.</span>
unfilled(X,Y) :- bottle(B,X,Y), not filled(X,Y).

<span class="comment">% The number of filled cells agrees with the given predicate.</span>
<span class="comment">% It cannot be the case that <code>xvalue(Y,V)</code>, but I don't know that the number of filled cells on that column is <code>V</code>.</span>
:- xvalue(Y,V), not #count{ X : filled(X,Y) } = V.
<span class="comment">% It cannot be the case that <code>yvalue(X,V)</code>, but I don't know that the number of filled cells on that row is <code>V</code>.</span>
:- yvalue(X,V), not #count{ Y : filled(X,Y) } = V.

<span class="comment">% Two constraints expressing gravity in terms of filled and unfilled.</span>
:- bottle(B,X1,Y1), bottle(B,X2,Y2), ysucc(Y1,Y2), filled(X1,Y1), unfilled(X2,Y2).
:- bottle(B,X1,Y), bottle(B,X2,Y), filled(X1,Y), unfilled(X2,Y), X1 ≠ X2.
</pre>

    <!-- <h5>Download</h5> -->
    <p>
      <i class="fas fa-file-download"></i> Download the entire ASP-model <a href="files/n06.asp">here</a>.
    </p>
  </div>
</div>

<script>
  var triggerTabListBottle = [].slice.call(document.querySelectorAll('#bottle-tab a'))
  triggerTabListBottle.forEach(function (triggerEl) {
    var tabTrigger = new bootstrap.Tab(triggerEl)

    triggerEl.addEventListener('click', function (event) {
      event.preventDefault()
      tabTrigger.show()
    })
  });
</script>
