<h2>Permutation Pattern Matching</h2>
<hr>
<h4>Description</h4>
<p>
  Permutation Pattern Matching (PPM) is an NP-complete pattern matching problem.
  Given a permutation T (the text) and a permutation P (the pattern) the question is whether there exists a matching of P into T.
</p>
<p>
  A matching is a subsequence of T that has the same relative order as P.
  For example the permutation T = 53142 (written in one-line representation) contains the pattern 231, since the subsequence 342 of T is order-isomorphic to 231 (i.e. the smallest element is in the third position, the second smallest in the first position and the largest in the second position).
</p>
<p>
  The original problem description (from the ASP Competition 2013) can be found <a href="https://www.mat.unical.it/aspcomp2013/FinalProblemDescriptions/PermutationPatternMatching">here</a>.
</p>

<h4>Input and output</h4>
Input:
<ul class="bi-ul">
  <li class="in"><code class="code">t/2A</code>: representation of T (in the example this would be <code class="code">t(1,5). t(2,3). t(3,1). t(4,4). t(5,2).</code>).</li>
  <li class="in"><code class="code">t/2A</code>: representation of P (in the example this would be <code class="code">p(1,2). p(2,3). p(3,1)</code>.).</li>
  <li class="in"><code class="code">patternlength/1</code>: The length of P (superfluous).</li>
</ul>
Output:
<ul class="bi-ul">
  <li class="out">
    The output predicate solution contains an encoding of the matching of P into T (if it exists) (in the example, it would be <code>solution(1,3). solution(2,4). solution(3,2).</code>.
  </li>
</ul>

<div class="on-page-nav nav nav-pills d-flex flex-row align-items-baseline justify-content-end" id="permutation-tab" role="tablist" aria-orientation="vertical">
  <h4 style="flex-grow:1;">Models</h4>
  <a class="nav-link active" id="permutation-IDP-tab" data-bs-toggle="pill" href="#permutation-IDP" role="tab" aria-controls="permutation-IDP" aria-selected="true">IDP</a>
  <a class="nav-link" id="permutation-ASP-tab" data-bs-toggle="pill" href="#permutation-ASP" role="tab" aria-controls="permutation-ASP" aria-selected="true">ASP</a>
</div>
<div class="tab-content mt-2">
  <div class="tab-pane fade show active" id="permutation-IDP" role="tabpanel" aria-labelledby="permutation-IDP-tab">
    <!-- <h5>About</h5> -->
    <p>
      In the IDP-model, we chose to replace the predicates <code>t</code> and <code>p</code> by functions <code>tf</code> and <code>pf</code> respectively (since in fact, they are functions).
      The problem then reduces to finding another function <code>index</code>: an order-respecting injection of <code>{1,...,n}</code> into <code>{1,...,m}</code>, where <code>n</code> is the length of <code>P</code> and <code>m</code> is the length of <code>T</code>.
      The actual pattern (solution) is the composition of <code>t</code> and <code>index</code> and should be order-isomorphic to <code>p</code>.
    </p>
    <!-- <h5>Model</h5>perm -->

<pre>
<span class="idpheader">vocabulary V{</span>
type number <span class="comment">//1..m</span>
type smallnumber isa number <span class="comment">//1..n</span>
tf(number) : number <span class="comment">//input</span>
pf(smallnumber) : number <span class="comment">//input</span>
index(smallnumber):number <span class="comment">//Indices of the mapped pattern</span>
solution(smallnumber):number <span class="comment">//The requested solution</span>
<span class="idpheader">}</span>
<span class="idpheader">theory T: V{</span>
<span class="comment">//The index function should respect the order.</span>
∀ n1 n2: n1 &lt; n2 ⇒ index(n1) &lt; index(n2).

<span class="comment">//Solution is the composition of tf and index.</span>
∀ x: tf(index(x)) = solution(x).

<span class="comment">//Solution is order-isomorphic to pf.</span>
∀ n1 n2: pf(sn1) &lt; pf(sn2) ⇒ solution(sn1) &lt; solution(sn2).
<span class="idpheader">}</span>
</pre>

    <!-- <h5>Download</h5> -->
    <p>
      <i class="fas fa-file-download"></i> Download the entire IDP-model (including preprocessing of ASP factlists) <a href="files/n01.idp">here</a>.
    </p>
  </div>
  <div class="tab-pane fade" id="permutation-ASP" role="tabpanel" aria-labelledby="permutation-ASP-tab">
    <!-- <h5>About</h5> -->
    <p>
      The ASP-model chooses to search for a predicate <code>subt/3</code>.
      This predicate encodes the <code>index</code>-function from the IDP modeling when restricted to its first two arguments,
      <code>t</code> when restricted to its last two arguments, and <code>solution</code> when restricted to its first and last argument.
    </p>
    <!-- <h5>Model</h5> -->

<pre>
<span class="comment">% Define which are valid indices: 1..patternlength</span>
kval(1).
kval(N+1) :- kval(N), N &lt; L, patternlength(L).

<span class="comment">% Tell the solver that for every index <code>K</code> one tuple <code>I,E</code> from <code>t</code> should be chosen. For this tuple, <code>subt(K,I,E)</code> is true. </span>
1 ≤ { subt(K,I,E) : t(I,E) } ≤ 1 :- kval(K), patternlength(L).

<span class="comment">% Constraint expressing that the restriction of subt to the first two arguments respects the order. </span>
:- subt(K1,I1,_), subt(K2,I2,_), K1 &lt; K2, I1 ≥ I2.

<span class="comment">% Solution is obtained from subt by dropping the second argument. </span>
solution(K,E) :- subt(K,_,E).

<span class="comment">% Solution is order-isomorphic to pf.</span>
:- solution(K1,ET1), solution(K2,ET2), p(K1,EP1), p(K2,EP2), ET1 &lt; ET2, EP1 ≥ EP2.
</pre>

    <!-- <h5>Download</h5> -->
    <p>
      <i class="fas fa-file-download"></i> Download the entire ASP-model <a href="files/n01.asp">here</a>.
    </p>
  </div>
</div>

<script>
  var triggerTabListPermutation = [].slice.call(document.querySelectorAll('#permutation-tab a'))
  triggerTabListPermutation.forEach(function (triggerEl) {
    var tabTrigger = new bootstrap.Tab(triggerEl)

    triggerEl.addEventListener('click', function (event) {
      event.preventDefault()
      tabTrigger.show()
    })
  });
</script>
