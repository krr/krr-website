<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Knowledge Representation and Reasoning - KU Leuven - Matthias van der Hallen">

    <title>IDP</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/custom.css" rel="stylesheet">
    <!-- Custom styles for krr website -->
    <link href="css/krr.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" crossorigin="anonymous">
  </head>

  <body>
    <header>
      <div id="navbar"></div>
    </header>

    <main role="main" class="container">
      <div class="row">
        <div class="col-12 mt-4">
          <h2>IDP</h2>
          <h4>Table of Contents</h4>
          <hr>
          <ul class="nav sub-nav flex-column justify-content-end nav-pills">
            <li class="nav-item">
              <a class="nav-link" href="#Introduction">Introduction</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#Examples">Examples</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#Usage">Getting started</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#Help">Help</a>
            </li>
          </ul>
      </div>
    </div>
      <!-- <nav id="IDP-nav" class="navbar navbar-dark bg-dark">
        <ul class="nav nav-pills">
          <li class="nav-item">
            <a class="nav-link" href="#Examples">Examples</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#Help">Help</a>
          </li>
        </ul>
      </nav> -->
      <a id="Introduction"></a>
      <div class="row">
        <div class="col-12 mt-4">
          <h4>Introduction</h4>
          <hr>
          <p>
            IDP is a knowledge Base System (KB-system) for the FO(·) language.
            A Knowledge Base system is a system that supports multiple forms of inferences for the same Knowledge Base.
            FO(·) is an extension of first-order logic (FO) with types, aggregates, inductive definitions, bounded arithmetic, partial functions, etc.
            For more information on the FO(·) language itself, please see <a href="fodot.html">this page</a>.
          </p>

          <p>
            Using IDP, it is possible to model systems and problems, both from the real world as several artificial intelligence research problems.
            Solutions to these problems can then be found using one or more inference techniques.
          </p>
        </div>
      </div>


      <div class="row">
        <div class="col-12 mt-4">
          <a id="Examples"></a>
          <h4>Examples</h4>
          <hr>
          <p>One class of real world problems that serves as a nice introduction to IDP is a scheduling problem. Imagine for example a hospital schedule where employees are assigned to certain departments. A good schedule should meet certain rules:</p>
          <ul>
            <li>Every employee works on exactly one department.</li>
            <li>At every department, at least a certain minimal occupance is present</li>
          </ul>

          <p>The first step is defining a vocabulary containing all symbols we'll make statements about:</p>

          <pre class="idp-code">
vocabulary Work {
    type Employee
    type Department
    type Threshold isa int
    WorksOn(Employee, Department)
    MinimalOccupance(Department) : Threshold
}</pre>

          <p>Next, we need a theory that describes the conditions and constraints for a correct planning.</p>

          <pre class="idp-code">
theory SchedulingRules : Work {
    !e[Employee] : ?1 d[Department] : WorksOn(e, d).
    !d[Department] : #{e[Employee] : WorksOn(e, d)} >= MinimalOccupance(d).
}</pre>

          <p>Finally, we must specify which employees and departments exist, using a structure:</p>

          <pre class="idp-code">
structure Hospital : Work {
    Employee ={e1;e2;e3;e4;e5}
    Department = {d1;d2;d3}
    Threshold = {1..5}
    MinimalOccupance = {d1->1;d2->2;d3->1}
}</pre>

          <p>
          Now that the problem and its constraints are expressed, several inference techniques can be used to solve different questions:</p>
          <ul>
          <li>Using model expansion inference, it is possible to generate a scheduling for the hospital.</li>
          <li>Using satisfiability inference, it is possible to check the validity of a certain schedule.</li>
          <li>Using optimization inference, it is possible to generate a schedule that is optimal according to some quantifiable criterium.</li>
          </ul>
          <p>
            Feel free to play around with <a href="//dtai.cs.kuleuven.be/krr/idp-ide/?src=b81053f9f88c191d86f8">this example</a> in our online editor.
            The following table gives an overview of the other examples that are available and which can help get you familiar with the IDP system.
            Each example has a short explanation, and can be opened and executed in the online editor by clicking on the play button.
          </p>

          <ul>
          	<li><a href="//dtai.cs.kuleuven.be/krr/idp-ide/?example=basic/1-HelloWorld">Hello World</a><span class="keywords"> &mdash;	 Illustrates: Lua scripting</span></li>
          	<li><a href="//dtai.cs.kuleuven.be/krr/idp-ide/?example=basic/2-Arithmetic">Arithmetic</a><span class="keywords"> &mdash; Illustrates: Model expansion, arithmetic</span></li>
          	<li><a href="//dtai.cs.kuleuven.be/krr/idp-ide/?example=basic/3-TransitiveClosure">Reachability</a><span class="keywords"> &mdash; Illustrates: Model expansion, inductive definitions</span></li>
          	<li><a href="//dtai.cs.kuleuven.be/krr/idp-ide/?example=basic/4-MapColouring">Map coloring</a><span class="keywords"> &mdash; Illustrates: Model expansion, functions, interpretations, graph colouring</span></li>
                  <li><a href="//dtai.cs.kuleuven.be/krr/idp-ide/?example=basic/2-Linear">Linear constraints</a><span class="keywords"> &mdash; Illustrates: Model expansion, optimization, arithmetic</span></li>
          	<li><a href="//dtai.cs.kuleuven.be/krr/idp-ide/?example=basic/5-Nqueens">N-queens</a><span class="keywords"> &mdash; Illustrates: Xsb usage, preprocessing definitions, Lua scripting, options</span></li>
          	<li><a href="//dtai.cs.kuleuven.be/software/idp/examples/minagg">Minimization &amp; Aggregates</a><span class="keywords"> &mdash; Illustrates: Aggregates, optimization</span></li>
          	<li><a href="//dtai.cs.kuleuven.be/krr/idp-ide/?example=basic/2-Aggregates">Aggregates 2</a><span class="keywords"> &mdash; Illustrates: Aggregates</span></li>
          	<li><a href="//dtai.cs.kuleuven.be/krr/idp-ide/?example=basic/6-MaxStillLife">Game of life</a><span class="keywords"> &mdash; Illustrates: Arithmetic, optimization</span></li>
          	<li><a href="//dtai.cs.kuleuven.be/krr/idp-ide/?example=basic/7-MaxStillLife_Constructors">Game of life 2</a><span class="keywords"> &mdash; Illustrates: Constructors (Herbrand interpretations),
          arithmetic, definitions</span></li>
          	<li><a href="//dtai.cs.kuleuven.be/krr/idp-ide/?example=basic/8-Einsteinpuzzle">Einstein puzzle</a><span class="keywords"> &mdash; Illustrates: Constructors (Herbrand interpretations), partial interpretations, first-order logic</span></li>
          	<li><a href="//dtai.cs.kuleuven.be/krr/idp-ide/?example=basic/9-options">Options</a><span class="keywords"> &mdash; Illustrates: Options</span></li>
          	<li><a href="//dtai.cs.kuleuven.be/krr/idp-ide/?example=basic/stable-semantics">Stable semantics</a><span class="keywords"> &mdash; Illustrates: Stable models</span></li>
          	<li><a href="//dtai.cs.kuleuven.be/krr/idp-ide/?example=basic/9-Iterated-induction">Block world</a><span class="keywords"> &mdash; Illustrates: Linear time calculus, temporal domains, progression, simulation, iterated inductive definitions, proving invariants</span></li>
          	<li><a href="//dtai.cs.kuleuven.be/krr/idp-ide/?example=basic/10-Concrete_Delivery">Concrete Delivery</a><span class="keywords"> &mdash; Illustrates: Temporal domains, complex constraints</span></li>
          	<li><a href="examples/sensitiveflow">Security: Modeling sensitive information flow</a><span class="keywords"> &mdash; Illustrates: Inductive definitions, complex applications</span></li>
                  <li><a href="//dtai.cs.kuleuven.be/krr/idp-ide/?example=basic/12-Propagate-Tables">Propagation & Predicate Table Access</a><span class="keywords"> &mdash; Illustrates: Propagation, Lua structure manipulation</span></li>
          </ul>

          <h4 id="Usage">Getting Started</h4>
          <hr>
          <p>
            We provide a resource limited <a href="https://dtai.cs.kuleuven.be/krr/idp-ide/">online version</a> of the IDP IDE and system.
            You can also download binaries for Mac and Linux for IDP system and the IDP editor below.
          </p>
          <h5>IDP System</h5>
          <p>For people who are running windows, or who have trouble with the mac release, we advise using the dockerized version of idp3: <a href="https://dtai.cs.kuleuven.be/krr/files/releases/idp/README-DOCKER.md">Guidelines<a></p>
          <ul class="timelinelist">
            <li class="latest">
              <span class="version">IDP-3.7.0</span>
              <a href="//dtai.cs.kuleuven.be/krr/files/releases/idp/idp-linux-latest.tar.gz">linux binaries</a> - <a href="//dtai.cs.kuleuven.be/krr/files/releases/idp/3.7.0/idp-3.7.0-Darwin.tar.gz">Mac binaries</a>
            </li>
          </ul>

          <h5>IDP IDE</h5>
          While any text editor can be used to edit IDP theories, we provide an in-browser IDE for the IDP-system providing font ligatures, syntax highlighting, etc.
          The dockerized version mentioned above comes with this editor pre-packaged, and you can download Linux and Mac binaries for this editor below.
          <ul class="timelinelist">
            <li class="latest">
              <span class="version">IDP-IDE 2.0.1</span>
              <a href="//dtai.cs.kuleuven.be/krr/files/releases/idp-ide/latest/webID-2.0.1.tar.gz">linux binaries</a> - <a href="//dtai.cs.kuleuven.be/krr/files/releases/idp-ide/latest/webID-2.0.1-Darwin.tar.gz">Mac binaries</a>
            </li>
          </ul>

          <h5>License information</h5>
          <p>
            The IDP system is published under <a href="https://opensource.org/licenses/lgpl-3.0.html">LGPL (version 3)</a>.
            It depends on the XSB Library, which is published under the <a href="https://www.gnu.org/licenses/old-licenses/lgpl-2.0.en.html">GNU library general public license (version 2)</a>, its predecessor.
            This means that one can safely and legally use IDP binaries and code in software for internal use in companies.
          </p>

          <p>
            For other use, see the <a href="https://opensource.org/licenses/lgpl-3.0.html">definition of LGPL v3</a> or contact us.
          </p>

          <h4 id="Help">Help</h4>
          <hr>
          <p>Beyond the above examples, we also provide a <a href="//dtai.cs.kuleuven.be/krr/files/TutorialIDP.pdf">tutorial</a> and a <a href="//dtai.cs.kuleuven.be/krr/files/bib/manuals/idp3-manual.pdf">manual</a>.
            If you have a question that the manual does not answer, you can direct your question to the <a href="https://www.cs.kuleuven.be/cgi-bin/e-post.pl?epost=krr">development team</a>. If you encountered bugs, you can add them to our <a href="https://bitbucket.org/krr/idp/issues">issues tracker</a>.
          </p>
        </div>
      </div>
    </main>

    <footer class="footer">
      <div id="footer"></div>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <!-- <script>window.jQuery || document.write('<script src="js/jquery-slim.min.js"><\/script>')</script> -->
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- <script src="js/bibtex.js"></script> -->
    <script src="js/krr.js"></script>
  </body>
</html>
