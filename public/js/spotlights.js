$(document).on('click', '.open-spotlight', function(){
  var name = $(this).data('name');
  var src  = $(this).data('url');
  $("#spotlight-name").text(name);
  $("#youtube-iframe").attr('src', src);
  $('#spotlight-modal').modal('handleUpdate');
});
