<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Knowledge Representation and Reasoning - KU Leuven - Matthias van der Hallen">

    <title>FO(&middot;)</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/custom.css" rel="stylesheet">
    <!-- Custom styles for krr website -->
    <link href="css/krr.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" crossorigin="anonymous">
  </head>

  <body>
    <header>
      <div id="navbar"></div>
    </header>

    <main role="main" class="container">
      <div class="row">
        <div class="col-12 mt-4">
          <h2>FO(&middot;)</h2>
          <p class="lead">
            This page provides a basic introduction to the family of FO(·) knowledge representation languages as used by the <a href="idp.html">IDP system</a>.
            This introduction is mainly informal, for a formal scientific introduction we refer you to our <a href="https://lirias.kuleuven.be/retrieve/518656">overview paper</a>.
          </p>

          <p>The FO(·) knowledge representation language family introduces extensions of first-order logic. These extensions are:</p>
          <ul>
          	<li>(Inductive) definitions</li>
          	<li>Aggregates</li>
          	<li>Types</li>
          </ul>

          <p>
            For the remainder of this introduction, we'll focus on the extension of FO with inductive definitions, or FO(ID) for short.
          </p>

          <h3>FO(ID)</h3>
          <hr>

          <p>
            The FO(ID) language integrates key concepts from nonmonotonic reasoning into a classical setting, resulting in an intuitive and expressive knowledge representation language, suitable for practical applications.
          </p>
          <p>
            Definitions are essentially a constructive kind of knowledge, specifying how an interpretation of the open predicates can be extended to the defined predicates. As such, they provide additional expressiveness to first-order logic without adding significant computational complexity. Together with the extensions for aggregates, types, partial functions, etc., inductive definitions are supported by the IDP system as FO(·)<sup>IDP</sup> (sometimes referred to as FODOT).
          </p>

          <blockquote class="blockquote text-right">
            <p>
              Over the past 25 or so years, there have been two distinct generally accepted semantics for logic programs: the well-founded semantics and the stable model (or Answer Set) semantics. There seems to be agreement that the two semantics, which are mutually inconsistent, are both important and appropriate for different kinds of applications. But it would be much nicer if there were one general semantic theory that included both in one coherent system. This is what I think FO(ID) does.
            </p>
            <footer class="blockquote-footer">Professor David Warren in a <cite title="Source Title"><a href="https://www.linkedin.com/pulse/one-semantics-logic-programming-david-warren">LinkedIn Editorial</a></cite><img src="https://dtai.cs.kuleuven.be/krr/DavidWarren.jpg" style="width: 5%; margin-left: 10px"/></footer>
          </blockquote>

          <h4>Inductive definitions</h4>
          <hr>
          <p>
            A definition, in the formal sense, is something that tells you exactly which tuples belong to a certain relation. An example of such a definition might be "We define a triangle to be a polygon with exactly three sides". Inductive definitions are those definitions which define a relation in terms of itself, i.e., whether a certain tuple belongs to the defined relation depends on whether some other tuple belongs to this same relation. Clearly, the example definition of the triangle is not inductive. Yet, inductive definitions are ubiquitous: for example, the natural numbers can be defined (inductively) as:
          </p>
          <ul>
          	<li>0 is a natural number.</li>
          	<li>The successor x+1 of a natural number x is a natural number as well.</li>
          </ul>

          <h4>Why study inductive definitions?</h4>
          <hr>

          <p>Besides the natural numbers, many other concepts can be defined inductively. For instance the <strong>frame axiom</strong> states that a property holds if it already held previously and was not terminated. Likewise, a state is <strong>reachable</strong> if it is the result of executing an action in a state which was already reachable. All in all, it is quite clear that inductive definitions are a distinctive and important kind of knowledge. Yet in first-order logic, it is impossible to define a transitive closure. Switching to second-order logic does not provide a general and uniform way for expressing inductive definitions. This is why FO(ID) introduces a specific language construct to express inductive definitions.</p>

          <h4>Writing inductive definitions</h4>
          <hr>

          <p>
            FO(ID) keeps inductive definitions as close to the way we naturally express them as possible. Lets look at an example where we have a relation (e.g., representing the edges in a graph) and inductively define its transitive closure (e.g., the reachability relation between nodes of the graph). In a mathematical text, we would likely write the following:
          </p>

          <div style="margin: 15px; border-left: .2em solid #999999; padding-left: 10px;">The transitive closure TC(R) of a relation R is defined inductively as, for all x, y:
          <ul>
          	<li>If (x,y) belongs to R, then (x,y) belongs to TC(R).</li>
          	<li>If there exists a z, such that (x,z) and (z,y) belong to TC(R), then (x,y) belongs to TC(R).</li>
          </ul>
          </div>
          <p>
            In FO(ID), we would write this as follows:
          </p>

          <div style="margin: 15px; border-left: .2em solid #999999; padding-left: 10px;">define {
          <div style="padding-left: 10px;">∀x,y TC(x,y) ← R(x,y).</div>

          <div style="padding-left: 10px;">∀x,y TC(x,y) ← ∃z TC(x,z) ∧ TC(z,y).</div>
          }</div>
          <p>
            You likely notice the strong similarities between these two formulations. We can break down the different parts of the FO(ID) formulation and indicate their counterpart in the mathematical formulation as follows:
          <p>
          <ul>
          	<li>The curly braces { and }, together with the (optional) define keyword, indicate the start of an inductive definition.</li> <!-- just as we do for the transitive closure mathematically.-->
          	<li>The definitional implication symbol ← indicates that if the conditions on the right are satisfied, then some tuple belongs to the relation being defined. This corresponds to the if ... then.</li>
          	<li>A set of different rules of the form ∀x P(t) ← φ., corresponding to the different rules in the mathematical formulation.</li>
          </ul>
          <p>
            Each rule of form ∀x P(t) ← φ. is to be read as follows: for all x, if φ, then by definition P(t) holds.
            We call the atom P(t) the head of the rule, whereas formula φ is the body of the rule.
            A predicate that appears in the head of at least one rule of a definition, is said to be defined by this definition.
            All other predicates are called open.
            In the example, TC is the only defined predicate, while R is the only open.
          </p>

          <h4>FO(ID) in practice</h4>
          <hr>
          <p> Inductive definition have a great effect on the expressivity of logic systems due to the way they naturally occur in problems and express concepts that transcend first order, without influencing performance negatively. They can be computed, both under stable and well-founded semantics, in an efficient way using the IDP system.
          </p>
          </div>
          </div>
          </div>
        </div>
      </div>
    </main>

    <footer class="footer">
      <div id="footer"></div>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <!-- <script>window.jQuery || document.write('<script src="js/jquery-slim.min.js"><\/script>')</script> -->
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- <script src="js/bibtex.js"></script> -->
    <script src="js/krr.js"></script>
  </body>
</html>
